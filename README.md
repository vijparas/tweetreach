# README #

### What is this repository for? ###

This repository maintains the code for getting the reach of a particular tweet by fetching number of retweets 
and sum of count of followers of users who has retweeted the tweet. It maintains  record for each tweet for 2 hours. 
In 2 hours time-frame the reach will be fetched from database.

Version 1.0

# How do I get set up? #

Summary - The Project has been developed in Laravel so we need to make some configuration to make it work.

Configuration - 
Add Following three keys into your .env file
oauth_access_token='OAUTH_ACCESS_TOKEN'
oauth_access_token_secret='oauth_access_token_secret'
consumer_secret='consumer_secret'
consumer_key='consumer_key'

### Modification in .env files
#### Update 
DB_DATABASE='your database name'
DB_USERNAME='your database username'
DB_PASSWORD='your database password'

### Set 
QUEUE_DRIVER=database

# Dependencies
Install Dependencies by runnig composer-update

# Database Setup
run php artisan migrate

# Tests
To Run Tests 
run phpunit command in project folder

# How It Works
Whenever you post a link of tweet application checks if tweet has already being fetched. If it has already being fetched 
we do not need to hit twitter api again rater we will take the reach and retweets from database.
We maintain tweets in database for two hours. When entry for tweet is created in database a subsequent job is created which will
delete tweet after 2 hours. 
If tweet is not found in database twitter api is called.
Request to twitter api is maintained in OAuthProxyService Class in App\Services namespace.

# Job will be executed only if we are listening to queue.

Repository Maintained By
Paras Vij (vijparas@gmail.com)


