<?php

namespace Tests\Unit;
use Log;
use Tests\TestCase;
use App\TweetReach;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TwitterApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUrl()
    {
        $response = $this->get('/tweet/reach');
        $response->assertStatus(200);
    }

    public function testValidTweet(){
    	  $response = $this->json('POST', '/tweet/reach', ['url' => 'https://twitter.com/defcon/status/905556338264047617']);
    	  $response->assertJsonFragment(['success'=>true]);

    }

    public function testInvalidUrl(){
    	$response = $this->json('POST', '/tweet/reach', ['url' => '90555633824047617']);
    	  $response->assertJsonFragment(['success'=>false]);
    }

    public function testInvalidTweet(){
    	$response = $this->json('POST', '/tweet/reach', ['url' => 'https://twitter.com/defcon/status/90555633824047617']);
    	  $response->assertJsonFragment(['success'=>false]);
    }

    public function testFetchFromApi(){
    	TweetReach::where('tweet_id','914592637591670786')->delete();
    	$response = $this->json('POST', '/tweet/reach', ['url' => 'https://twitter.com/defcon/status/914592637591670786']);
    	  $response->assertJsonFragment(['source'=>'Twitter']);
    }
    public function testFetchFromDatabase(){
    	$tweetDatabase=TweetReach::first();
    	if(count($tweetDatabase)!=0){
    		$response = $this->json('POST', '/tweet/reach', ['url' => 'https://twitter.com/defcon/status/'.$tweetDatabase->id]);
    	  $response->assertJsonFragment(['source'=>'Database']);	
    	}
    	else{
    		$tweet=TweetReach::create(['tweet_id'=>'914592637591670786','retweets'=>0,'reach'=>0])->id;

    		$response = $this->json('POST', '/tweet/reach', ['url' => 'https://twitter.com/defcon/status/914592637591670786']);
    	  $response->assertJsonFragment(['source'=>'Database']);
    	  TweetReach::where('id',$tweet)->delete();
    	
    	}
    	
    }

   


}
