<?php

namespace App\Jobs;
use App\TweetReach;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteTweet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $tweetReach;
    public function __construct(TweetReach $tweetReach)
    {
        $this->tweetReach=$tweetReach;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TweetReach $tweetReach)
    {
        
    }
}
