<?php

namespace App\Jobs;
use App\TweetReach;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Repositories\TweetReachRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
class DeleteTweetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $tweetReach;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TweetReach $tweetReach)
    {
        $this->tweetReach=$tweetReach;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TweetReach $tweetReach)
    {
        TweetReach::where('id',$this->tweetReach->id)->delete();    
    }
}
