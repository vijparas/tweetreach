<?php
namespace App\Repositories;
use App\TweetReach;
use App\Jobs\DeleteTweetJob;
use Carbon\Carbon;
class TweetReachRepository{
	public function checkTweet($tweetId){
		$reach=TweetReach::where('tweet_id',$tweetId)->first();
		return $reach;
	}
	/**
	 * [saveTweet] 
	 * Save Tweet and create a job that will delete tweet after 2 hrs]
	 * @param  [array] $data [description]
	 * @return [type]       [description]
	 */
	public function saveTweet($data){
		$tweet=new TweetReach();
		$tweet->tweet_id=$data['tweet_id'];
		$tweet->retweets=$data['retweets'];
		$tweet->reach=$data['reach'];
		$tweet->save();
		DeleteTweetJob::dispatch($tweet)->delay(Carbon::now()->addMinutes(120));
		
		
	}

	

}
?>