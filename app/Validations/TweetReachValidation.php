<?php
namespace App\Validations;
use Validator;
class TweetReachValidation{
	public function validateUrl(array $data){
		return Validator::make($data, [
   			'url' => 'required|url',
			]);
	}
}
?>