<?php
namespace App\Services;
class OAuthProxyService{
	private $config = [
        
        'base_url' => 'https://api.twitter.com/1.1/'
    ];
    private $requestMethod="GET";
	private $oauth_access_token; 
	private $oauth_access_token_secret; 
	private $consumer_key;
	private $consumer_secret;
	public function __construct(){
		$this->oauth_access_token=env('oauth_access_token','70178756-lqBeGOQTijHDi3ugVynWswkMPLAfN5ctjER5Ua4s6');
		$this->oauth_access_token_secret=env('oauth_access_token_secret','KyFstAUCHCUr72BuHfc20L0Y4hrVwiTMSl6HwFP1nqfUE');
		$this->consumer_secret=env('consumer_secret','2y8sGyjp712Ycy0hq5vDKDc19VMA7yoOl3vi1XJYCHPh3bn3W0');
		$this->consumer_key=env('consumer_key','vlZNaZNilf7ibabapMVZEY2wx');
		
	}
	/**
	 * [makeRequest description]
	 * Setup configuration for sending request to twitter api
	 * @param   $apiUrl
	 
	 */
	public function makeRequest($apiUrl){
		  $fullUrl=$this->config['base_url'].$apiUrl;
		  $url_parts = parse_url($apiUrl);
		  $url_arguments=[];
         if(isset($url_parts['query'])){
             parse_str($url_parts['query'], $url_arguments);
         }
		  $apiUrl=$this->config['base_url'].$url_parts['path'];
		  $baseInfoString=$this->buildInfoString($apiUrl,$url_arguments);
		  $compositeKey=$this->generateCompositeKey();
		  $headers=$this->OAuthHeaders();
		  $headers['oauth_signature']=$this->generateSignature($baseInfoString,$compositeKey);
		  $header=[$this->buildAuthorizationHeader($headers), 
            'Expect:'];

		  $options = [
            CURLOPT_HTTPHEADER => $header,
            //CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $fullUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $result = curl_exec($feed);
        $info = curl_getinfo($feed);
        curl_close($feed);
        $response['http_code']=$info['http_code'];
        $response['result']=$result;
        return $response;
	}

	public function OAuthHeaders(){
		return $oauth = [
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $this->oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        ];
	}
	
	public function buildAuthorizationHeader($headers){
		
		$header='Authorization: OAuth ';
		$returnHeader=array();
		foreach ($headers as $key => $value) {
			$returnHeader[]= "$key=\"" . rawurlencode($value) . "\"";		
		}
		return $header.=implode(", ",$returnHeader);	
	}
	public function buildInfoString($url,$urlArguments){
		$oauthHeaders=array_merge($this->OAuthHeaders(),$urlArguments);
		$baseString=array();
		 ksort($oauthHeaders);
		foreach($oauthHeaders as $headerKey=>$headerValue){
            $baseString[] =  "$headerKey=" . rawurlencode($headerValue);
        }
        return $this->requestMethod."&".rawurlencode($url)."&".rawurlencode(implode('&',$baseString));
	}

	public function generateCompositeKey(){
		return rawurlencode($this->consumer_secret).'&'.rawurlencode($this->oauth_access_token_secret);
	}

	public function generateSignature($baseInfo,$compositeKey){
		return base64_encode(hash_hmac('sha1',$baseInfo,$compositeKey,true));
	}



}
?>