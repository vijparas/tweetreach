<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\DeleteTweetJob;
use App\Services\OAuthProxyService;
use App\Validations\TweetReachValidation;
use App\Repositories\TweetReachRepository;
use App\TweetReach;
class TweetReachController extends Controller
{
    const retweetApiUrl="/statuses/retweets/";
    const followersApiUrl="/followers/ids.json";
    private $oAuthProxyServiceObject; // Object To Proxy Wrapper
    private $tweetReach=0;
    private $tweetReachRepositoryObject;
    private $tweetReachValidationObject;
    private $tweetId;
    private $retweetCount;
    private $apiUrl;

    public function __construct()
    {
        $this->oAuthProxyServiceObject=new OAuthProxyService();
        $this->tweetReachRepositoryObject=new TweetReachRepository();
        $this->tweetReachValidationObject=new TweetReachValidation();
    }
    /**
     * [index description]
     * @return [view] 
     */
    public function index()
    {
        return view('searchTweets.tweetReachCount');
    }
    /**
     * [getTweetReach description]
     * @param  Request $request 
     * @return [json]           [return json response]
     */
    public function getTweetReach(Request $request){
        $data = $request->all();
        $url = $request['url'];
        $response=$this->validateUrl($url);
        if($response!=1){
           return json_encode($response);
        }
        $this->setTweetId($url);
        $this->apiUrl=$this->generateUrl();
        $response=$this->calculateReach();    
        return $response;
    }
    /**
     * [validateUrl description]
     * @param  [type] $url [Url]
     * 
     */
     private function validateUrl($url){
        $validationData['url']=$url;
        $validationResults=$this->tweetReachValidationObject->validateUrl($validationData);
        if($validationResults->fails()){
            $response['message']=$validationResults->errors()->all();
            $response['success']=false;
            return $response;
        }
        else{
            return 1;
        }
    }
    /**
     * [setTweetId set id of requested tweet]
     * @param  $url
     */
    private function setTweetId($url){
        $urlArray=explode("/",$url);
        $this->tweetId=end($urlArray);
    }
    /** [generateUrl for twitter API] */
    private function generateUrl(){
        return self::retweetApiUrl.$this->tweetId.".json";
    }
   /**
    * [calculateReach description]
    * Check if tweet id is present in database.
    * If tweet id is present fetch record from database.
    * If tweet id is not present call fetchFromApi method.
    * 
    * @return [json] [description]
    */
    private function calculateReach(){
        if($this->checkDatabase()==-1){
            $response=$this->fetchFromApi();
        }
        else{
            $response['success']=true;
            $response['message']="Tweet Has a Reach of ".$this->tweetReach." with ".$this->retweetCount." retweets. Data Has Been Fetched From Database.";
            $response['source']="Database";
        }
        return json_encode($response);

    }
    /**
     * [checkDatabase Make a request to repository to check if tweet is present]
     * @return [integer] [1 is return if record is present otherwise -1 is returned]
     */
    private function checkDatabase(){
        $checkTweet=$this->tweetReachRepositoryObject->checkTweet($this->tweetId);
        if(count($checkTweet)>0){
            $this->tweetReach=$checkTweet->reach;
            $this->retweetCount=$checkTweet->retweets;
            return 1;
        }
        return -1;
    }
    /**
     * [fetchFromApi Prepare request for twitter api]
     * If request is succesful reach is calculated
     * In case request is not succesful error message is parsed from returned response
     * @return [array] [Message along with success is returned]
     */
    private function fetchFromApi()
    {
        $result=$this->oAuthProxyServiceObject->makeRequest($this->apiUrl);
        if ($this->responseOk($result['http_code'])==true) {
            $this->getReachCount($result['result']);
            $this->saveTweet();
            $response['message']="Tweet Has a reach of ".$this->tweetReach." with ".$this->retweetCount." retweets. Data Has Been Fetched From Twitter.";
            $response['success']=true;
            $response['source']="Twitter";
        } else {
            $response['message']=$this->returnTwitterError($result['result']);
            $response['success']=false;
        }
        return $response;
    }
    /**
     * [getReachCount description]
     * @param  [type] $retweets [Json array returned from twitter]
     *  tweetReach and retweet count variables are set
     */
    private function getReachCount($retweets)
    {
        $tweetsJson=json_decode($retweets);
        
        foreach ($tweetsJson as $tweetData) {
            $this->tweetReach += $tweetData->user->followers_count;
        }
        $this->retweetCount=$tweetsJson[0]->retweet_count;
    }
    /**
     * [saveTweet]
     * After fetching tweet details from twitter its details are saved for next 2 hours.
     * 
     */
    private function saveTweet(){
        $data['tweet_id']=$this->tweetId;
        $data['reach']=$this->tweetReach;
        $data['retweets']=$this->retweetCount;
        $tweet=$this->tweetReachRepositoryObject->saveTweet($data);

    }
    /**
     * [responseOk Check if request to twitter api is succesfull]
     * @param  [integer] $http_code [http code returned from twitter]
     * @return [boolean] 
     */
    private function responseOk($http_code)
    {
        if ($http_code==200) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * [returnTwitterError
     * If status is not 200 parse twitter erros
     * @param  [json] $response
     * @return [array]           [description]
     */
    private function returnTwitterError($response)
    {
        $errors=json_decode($response);
        $errorResponse=[];
        foreach ($errors as $error) {
            $errorResponse[]=$error[0]->message;
        }
       
        return $errorResponse;
    }

}
