@extends('template.main')
@section('title')
Get Tweet Reach
@endsection

@section('content')


<div class="container">
    <section>
        <div class="row">
            <header>Enter a tweet URL to find its reach</header>
        </div>
        <form method="POST" class="tweetUrlForm" action="{{route('find_reach')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <input type="text" name="url" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                    <button type="submit" id="submit" class="btn btn-success">Find</button>
                </div>
            </div>
        </form>
    </section>
    <section>
        <div class='row'>
            <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                <h3>Tweet Reach</h3>
            </div>
            <p id="result" class="text-center"></p>
        </div>
    </section>
</div>

@endsection
@section('page_scripts')
<script type="text/javascript" src="{{ URL::asset('resources/assets/js/jquery_form.min.js') }}"></script>
<script type="text/javascript">
    $('.tweetUrlForm').ajaxForm({
        // dataType: json,
        beforeSend: function () {
            $("#result").text("Loading Results");
            $("#submit").attr('disabled',true);
        },
        success: function (response) {
         var responseObject = jQuery.parseJSON(response);
         if (responseObject.success === false) {
            $('#result').text(responseObject.message);
        } else {
            $('#result').text(responseObject.message);
        }
    },
    complete: function () {
         $("#submit").attr('disabled',false);

    }

});
</script>
@endsection
