<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	@include('template.headLinks')
</head>
<body>
	@yield('content')
	@yield('page_scripts')
</body>
</html>